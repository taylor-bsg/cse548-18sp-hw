# FPGA accelerated computing using AWS F1

The goal of this homework is learn how to deploy hardware accelerators on a cloud FPGA.
Learn more about FPGA accelerated computing using AWS F1 instances [here](https://www.hotchips.org/wp-content/uploads/hc_archives/hc29/HC29.22-Tuesday-Pub/HC29.22.50-FPGA-Pub/HC29.22.544-AWS-F1-Pellerin-Amazon%20v4.pdf).

* Cloud FPGAs has demonstrated already being a powerful platform for:
    * [Genomics processing](https://aws.amazon.com/blogs/compute/accelerating-precision-medicine-at-scale/)
    * [Neural networks](http://www.mipsology.com/aws/)
    * [Financial real time risk analysis](http://maxeler.com/files/MaxelerAWSF1RTRisk.pdf)
    

## The hardware accelerator should:
* Take an input file with lowercase characters, convert them into uppercase, and write the result in an output file
    * Similar to the linux translator `tr '[:upper:]' '[:lower:]' < input.txt > output.txt`
```
cat input.txt

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum
```

```
cat output.txt

LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT, SED DO EIUSMOD TEMPOR
INCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA. UT ENIM AD MINIM VENIAM, QUIS
NOSTRUD EXERCITATION ULLAMCO LABORIS NISI UT ALIQUIP EX EA COMMODO CONSEQUAT.
DUIS AUTE IRURE DOLOR IN REPREHENDERIT IN VOLUPTATE VELIT ESSE CILLUM DOLORE EU
FUGIAT NULLA PARIATUR. EXCEPTEUR SINT OCCAECAT CUPIDATAT NON PROIDENT, SUNT IN
CULPA QUI OFFICIA DESERUNT MOLLIT ANIM ID EST LABORUM
```
* The hardware accelerator should be written in Verilog and inside the [hello world](https://github.com/aws/aws-fpga/blob/master/hdk/cl/examples/cl_hello_world/design/cl_hello_world.sv)
* File IO should be done in software in `test_hello_world.c`
* Translation should be done in hardware in `cl_hello_world.sv`

## Initial steps:

1. Follow this [tutorial](https://github.com/vegaluisjose/aws-fpga-notes/blob/master/README.md)
2. Copy the new [test_hello_world.c](https://bitbucket.org/taylor-bsg/cse548-18sp-hw/src/master/hw2/test_hello_world.c) to `<aws-fpga>/hdk/cl/examples/cl_hello_world/software/runtime`
3. Compile and run

## Tips

* The hello world design is built for swapping bytes in a 32-bit register. This functionality is implemented [here](https://github.com/aws/aws-fpga/blob/master/hdk/cl/examples/cl_hello_world/design/cl_hello_world.sv#L265-L282).
* The hello world design documentation can be found [here](https://github.com/aws/aws-fpga/blob/master/hdk/cl/examples/cl_hello_world/README.md)
* The AWS shell interface specification can be found [here](https://github.com/aws/aws-fpga/blob/master/hdk/docs/AWS_Shell_Interface_Specification.md)
    * This shell is basically what is going to be connected to your design and interface the host processor (Intel Xeon)
* A programmer's view of the custom logic can be found [here](https://github.com/aws/aws-fpga/blob/master/hdk/docs/Programmer_View.md)

## Turnin using [canvas](https://canvas.uw.edu/courses/1199347/assignments/4233041)

* Design `*Developer_CL.tar`
* New Verilog `cl_hello_world.sv`
* New Runtime `test_hello_world.c`
